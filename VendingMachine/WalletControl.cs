﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VendingMachine
{
    public partial class WalletControl : UserControl
    {
        private Wallet wallet;
        bool IsActive = true;

        public WalletControl(Wallet aWallet, bool IsActive)
        {
            InitializeComponent();

            wallet = aWallet;
            this.IsActive = IsActive;
            wallet.AppendedIntoWallet += new MoneyTransferEvent(wallet_Changed);
            wallet.TakedFromWallet += new MoneyTransferEvent(wallet_Changed);
            btn1rub.Tag = CoinNominal.n1rub;
            btn2rub.Tag = CoinNominal.n2rub;
            btn5rub.Tag = CoinNominal.n5rub;
            btn10rub.Tag = CoinNominal.n10rub;
            wallet_Changed(null);
        }

        void wallet_Changed(Dictionary<CoinNominal, uint> coins)
        {
            uint count = wallet.NominalCount(CoinNominal.n1rub);
            lbl1rub.Text = string.Format("1 руб - {0} шт", count);
            btn1rub.Enabled = IsActive && (count > 0);
            count = wallet.NominalCount(CoinNominal.n2rub);
            lbl2rub.Text = string.Format("2 руб - {0} шт", count);
            btn2rub.Enabled = IsActive && (count > 0);
            count = wallet.NominalCount(CoinNominal.n5rub);
            lbl5rub.Text = string.Format("5 руб - {0} шт", count);
            btn5rub.Enabled = IsActive && (count > 0);
            count = wallet.NominalCount(CoinNominal.n10rub);
            lbl10rub.Text = string.Format("10 руб - {0} шт", count);
            btn10rub.Enabled = IsActive && (count > 0);
        }

        private void btn_Click(object sender, EventArgs e)
        {
            CoinNominal nominal = (CoinNominal)((sender as Button).Tag);
            wallet.TakeFromWallet(nominal);
        }

    }
}
