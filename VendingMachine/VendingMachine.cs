﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VendingMachine
{
    public class VendingMachine
    {
        private List<Drink> m_drinks = new List<Drink>();
        private Wallet wallet = new Wallet();
        private uint paidinAmount = 0;

        public event EventHandler Changed;
        public event MoneyTransferEvent PayedChange;

        public VendingMachine()
        {
            // заполняем автомат напитками
            AppendDrink("Чай", 13, 10);
            AppendDrink("Кофе", 18, 20);
            AppendDrink("Кофе с молоком", 21, 20);
            AppendDrink("Сок", 35, 15);

            // заполняем автомат монетами
            wallet.Append(CoinNominal.n1rub, 100);
            wallet.Append(CoinNominal.n2rub, 100);
            wallet.Append(CoinNominal.n5rub, 100);
            wallet.Append(CoinNominal.n10rub, 100);
        }

        public uint PaidingAmount
        {
            get
            {
                return paidinAmount;
            }
        }

        public Wallet Wallet
        {
            get
            {
                return wallet;
            }
        }

        private void OnChanged()
        {
            if (Changed != null)
                Changed(this, EventArgs.Empty);
        }

        public void takeOneCoin(CoinNominal nominal)
        {
            paidinAmount += Wallet.GetAmountFromNominal(nominal);
            wallet.Append(nominal, 1);
            OnChanged();
        }

        public bool BuyDrink(Drink drink)
        {
            if (drink.Price > paidinAmount || drink.Quantity == 0)
                return false;
            drink.Quantity -= 1;
            paidinAmount -= drink.Price;
            OnChanged();
            return true;
        }

        public Dictionary<CoinNominal, uint> GetChange()
        {
            Dictionary<CoinNominal, uint> change = new Dictionary<CoinNominal, uint>();
            if (paidinAmount > 0)
            {
                GiveCoins(CoinNominal.n10rub, ref paidinAmount, ref change);
                GiveCoins(CoinNominal.n5rub, ref paidinAmount, ref change);
                GiveCoins(CoinNominal.n2rub, ref paidinAmount, ref change);
                GiveCoins(CoinNominal.n1rub, ref paidinAmount, ref change);
            }
            if (change.Count > 0)
            {
                if (PayedChange != null)
                    PayedChange(change); // сообщаем о выдаче сдачи
                OnChanged(); // сообщаем об изменениях
            }
            return change;
        }

        private void GiveCoins(CoinNominal nominal, ref uint amount, ref Dictionary<CoinNominal, uint> change)
        {
            uint coinNominal = Wallet.GetAmountFromNominal(nominal);
            uint requiredCount = amount / coinNominal;
            uint count = wallet.TakeFromWallet(nominal, requiredCount);
            amount -= coinNominal * count;
            change.Add(nominal, count);
        }

        public IEnumerable<Drink> drinks()
        {
            return m_drinks;
        }

        private void AppendDrink(string drinkName, uint drinkPrice, uint drinkQuantity)
        {
            Drink drink = new Drink(drinkName);
            drink.Price = drinkPrice;
            drink.Quantity = drinkQuantity;
            drink.Changed += Changed;
            m_drinks.Add(drink);
        }
    }
}
