﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VendingMachine
{
    public partial class Form1 : Form
    {

        Wallet wallet = new Wallet(); // создаём кошелёк покупателя
        VendingMachine vendingMachine = new VendingMachine(); // создаём автомат

        public Form1()
        {
            InitializeComponent();

            wallet.Append(CoinNominal.n1rub, 10);
            wallet.Append(CoinNominal.n2rub, 30);
            wallet.Append(CoinNominal.n5rub, 20);
            wallet.Append(CoinNominal.n10rub, 15);

            // создаём контрол для кошелька покупателя
            WalletControl userWallet = new WalletControl(wallet, true);
            groupBox1.Controls.Add(userWallet);
            userWallet.Location = new Point(5, 20);

            // создаём контрол для автомата
            VendingMachineControl VMControl = new VendingMachineControl(vendingMachine);
            groupBox2.Controls.Add(VMControl);
            VMControl.Location = new Point(5, 20);

            // обрабатыаем события перевода денег между кошельком покупателя и автоматом
            wallet.TakedFromWallet +=
                (coins) =>
                {
                    foreach (KeyValuePair<CoinNominal, UInt32> pair in coins)
                        for (int i = 0; i < pair.Value; i++)
                            vendingMachine.takeOneCoin(pair.Key);
                };

            vendingMachine.PayedChange +=
                (coins) =>
                {
                    foreach (KeyValuePair<CoinNominal, UInt32> pair in coins)
                        wallet.Append(pair.Key, pair.Value);
                };
        }
    }
}
