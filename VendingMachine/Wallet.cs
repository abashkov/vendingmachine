﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VendingMachine
{

    public enum CoinNominal { n1rub = 1, n2rub = 2, n5rub = 5, n10rub = 10 };

    public delegate void MoneyTransferEvent(Dictionary<CoinNominal, uint> coins);

    public class Wallet
    {
        private Dictionary<CoinNominal, UInt32> m_money = new Dictionary<CoinNominal, uint>();

        public event MoneyTransferEvent AppendedIntoWallet;
        public event MoneyTransferEvent TakedFromWallet;

        public Wallet()
        {
            m_money.Add(CoinNominal.n1rub, 0);
            m_money.Add(CoinNominal.n2rub, 0);
            m_money.Add(CoinNominal.n5rub, 0);
            m_money.Add(CoinNominal.n10rub, 0);
        }

        public void Append(CoinNominal nominal, uint count)
        {
            if (count == 0)
                return;
            m_money[nominal] += count;
            if (AppendedIntoWallet != null)
            {
                Dictionary<CoinNominal, UInt32> arg = new Dictionary<CoinNominal, UInt32>();
                arg.Add(nominal, count);
                AppendedIntoWallet(arg);
            }
        }

        public uint TakeFromWallet(CoinNominal nominal, uint count = 1)
        {
            if (m_money[nominal] == 0 || count == 0)
                return 0;
            uint takenCount = Math.Min(m_money[nominal], count);
            m_money[nominal] -= takenCount;
            if (TakedFromWallet != null)
            {
                Dictionary<CoinNominal, UInt32> arg = new Dictionary<CoinNominal, UInt32>();
                arg.Add(nominal, takenCount);
                TakedFromWallet(arg);
            }
            return takenCount;
        }

        public uint NominalCount(CoinNominal nominal)
        {
            return m_money[nominal];
        }

        public static uint GetAmountFromNominal(CoinNominal nominal)
        {
            return (uint)nominal;
        }
    }

}
