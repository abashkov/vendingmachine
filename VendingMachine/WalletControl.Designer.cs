﻿namespace VendingMachine
{
    partial class WalletControl
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl1rub = new System.Windows.Forms.Label();
            this.lbl2rub = new System.Windows.Forms.Label();
            this.lbl5rub = new System.Windows.Forms.Label();
            this.lbl10rub = new System.Windows.Forms.Label();
            this.btn1rub = new System.Windows.Forms.Button();
            this.btn2rub = new System.Windows.Forms.Button();
            this.btn5rub = new System.Windows.Forms.Button();
            this.btn10rub = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl1rub
            // 
            this.lbl1rub.AutoSize = true;
            this.lbl1rub.Location = new System.Drawing.Point(12, 14);
            this.lbl1rub.Name = "lbl1rub";
            this.lbl1rub.Size = new System.Drawing.Size(64, 13);
            this.lbl1rub.TabIndex = 0;
            this.lbl1rub.Text = "1 руб - 0 шт";
            // 
            // lbl2rub
            // 
            this.lbl2rub.AutoSize = true;
            this.lbl2rub.Location = new System.Drawing.Point(12, 45);
            this.lbl2rub.Name = "lbl2rub";
            this.lbl2rub.Size = new System.Drawing.Size(64, 13);
            this.lbl2rub.TabIndex = 1;
            this.lbl2rub.Text = "2 руб - 0 шт";
            // 
            // lbl5rub
            // 
            this.lbl5rub.AutoSize = true;
            this.lbl5rub.Location = new System.Drawing.Point(12, 76);
            this.lbl5rub.Name = "lbl5rub";
            this.lbl5rub.Size = new System.Drawing.Size(64, 13);
            this.lbl5rub.TabIndex = 2;
            this.lbl5rub.Text = "5 руб - 0 шт";
            // 
            // lbl10rub
            // 
            this.lbl10rub.AutoSize = true;
            this.lbl10rub.Location = new System.Drawing.Point(12, 107);
            this.lbl10rub.Name = "lbl10rub";
            this.lbl10rub.Size = new System.Drawing.Size(70, 13);
            this.lbl10rub.TabIndex = 3;
            this.lbl10rub.Text = "10 руб - 0 шт";
            // 
            // btn1rub
            // 
            this.btn1rub.Location = new System.Drawing.Point(99, 9);
            this.btn1rub.Name = "btn1rub";
            this.btn1rub.Size = new System.Drawing.Size(64, 23);
            this.btn1rub.TabIndex = 4;
            this.btn1rub.Text = "Взять";
            this.btn1rub.UseVisualStyleBackColor = true;
            this.btn1rub.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn2rub
            // 
            this.btn2rub.Location = new System.Drawing.Point(99, 40);
            this.btn2rub.Name = "btn2rub";
            this.btn2rub.Size = new System.Drawing.Size(64, 23);
            this.btn2rub.TabIndex = 5;
            this.btn2rub.Text = "Взять";
            this.btn2rub.UseVisualStyleBackColor = true;
            this.btn2rub.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn5rub
            // 
            this.btn5rub.Location = new System.Drawing.Point(99, 71);
            this.btn5rub.Name = "btn5rub";
            this.btn5rub.Size = new System.Drawing.Size(64, 23);
            this.btn5rub.TabIndex = 6;
            this.btn5rub.Text = "Взять";
            this.btn5rub.UseVisualStyleBackColor = true;
            this.btn5rub.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn10rub
            // 
            this.btn10rub.Location = new System.Drawing.Point(99, 102);
            this.btn10rub.Name = "btn10rub";
            this.btn10rub.Size = new System.Drawing.Size(64, 23);
            this.btn10rub.TabIndex = 7;
            this.btn10rub.Text = "Взять";
            this.btn10rub.UseVisualStyleBackColor = true;
            this.btn10rub.Click += new System.EventHandler(this.btn_Click);
            // 
            // WalletControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btn10rub);
            this.Controls.Add(this.btn5rub);
            this.Controls.Add(this.btn2rub);
            this.Controls.Add(this.btn1rub);
            this.Controls.Add(this.lbl10rub);
            this.Controls.Add(this.lbl5rub);
            this.Controls.Add(this.lbl2rub);
            this.Controls.Add(this.lbl1rub);
            this.Name = "WalletControl";
            this.Size = new System.Drawing.Size(178, 137);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl1rub;
        private System.Windows.Forms.Label lbl2rub;
        private System.Windows.Forms.Label lbl5rub;
        private System.Windows.Forms.Label lbl10rub;
        private System.Windows.Forms.Button btn1rub;
        private System.Windows.Forms.Button btn2rub;
        private System.Windows.Forms.Button btn5rub;
        private System.Windows.Forms.Button btn10rub;
    
    }
}
