﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VendingMachine
{
    public class Drink
    {
        public readonly string Name;
        uint price = 1;
        uint quantity = 0;

        public event EventHandler Changed;

        internal Drink(string aName)
        {
            Name = aName;
        }

        void OnChanged()
        {
            if (Changed != null)
                Changed(this, EventArgs.Empty);
        }

        public uint Price
        {
            get
            {
                return price;
            }
            set
            {
                uint newPrice = Math.Max(1, value);
                if (price == newPrice)
                    return;
                price = newPrice;
                OnChanged();
            }
        }

        public uint Quantity
        {
            get
            {
                return quantity;
            }
            set
            {
                if (quantity != value)
                {
                    quantity = value;
                    OnChanged();
                }
            }
        }
    }
}
