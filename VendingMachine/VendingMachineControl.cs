﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VendingMachine
{
    public partial class VendingMachineControl : UserControl
    {
        VendingMachine vendingMachine;
        List<Button> DrinkButton = new List<Button>();

        public VendingMachineControl(VendingMachine VM)
        {
            InitializeComponent();

            vendingMachine = VM;
            vendingMachine.Changed += new EventHandler(vendingMachine_Changed);
            CreateDrinkButtons();
            vendingMachine_Changed(this, EventArgs.Empty);

            // создаём контрол для кошелька автомата
            WalletControl VMWallet = new WalletControl(vendingMachine.Wallet, false);
            groupBox2.Controls.Add(VMWallet);
            VMWallet.Location = new Point(5, 20);

            vendingMachine_Changed(this, EventArgs.Empty);
        }

        void CreateDrinkButtons()
        {
            int i = 0;
            foreach (Drink drink in vendingMachine.drinks())
            {
                Button btn = new Button();
                btn.Size = new Size(270, 30);
                groupBox1.Controls.Add(btn);
                btn.Location = new Point(5, 100 + i * 40);
                btn.Tag = drink;
                UpdateButton(btn);
                btn.Click += new EventHandler(btn_Click);
                drink.Changed += new EventHandler(drink_Changed);
                i++;
            }
        }

        void btn_Click(object sender, EventArgs e)
        {
            Drink drink = ((sender as Button).Tag as Drink);
            if (vendingMachine.BuyDrink(drink))
                MessageBox.Show("Спасибо!", "Покупка");
            else
                MessageBox.Show("Недостаточно средств!", "Покупка");
        }

        void UpdateButton(Button btn)
        {
            Drink drink = (btn.Tag as Drink);
            btn.Text = string.Format("{0} - {1} руб. (осталось {2} шт.)", drink.Name, drink.Price, drink.Quantity);
            btn.Enabled = (drink.Quantity > 0);
        }

        void drink_Changed(object sender, EventArgs e)
        {
            Drink drink = (sender as Drink);
            Button btn = FindDrinkButton(drink);
            if (btn != null)
                UpdateButton(btn);
        }

        Button FindDrinkButton(Drink drink)
        {
            if (drink == null)
                return null;
            foreach (Button btn in DrinkButton)
                if (btn.Tag == drink)
                    return btn;
            return null;
        }

        void vendingMachine_Changed(object sender, EventArgs e)
        {
            lblAmount.Text = string.Format("Внесено: {0} руб", vendingMachine.PaidingAmount);
            btnChange.Enabled = (vendingMachine.PaidingAmount != 0);
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            vendingMachine.GetChange();
        }
    }
}
